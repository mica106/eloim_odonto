// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require inputmask
//= require jquery.inputmask
//= require pace/pace
//= require turbolinks
//= require jquery.turbolinks
//= require_tree .

// validações para a forma Novo Cadastro
$(document).ready(function(){
    $("#campoCPF").inputmask("999.999.999-99");
    $("#campoRG").inputmask("9999999999999");
    $("#campoData").inputmask("99/99/9999");
    $("#campoTelefone").inputmask("99-9999-9999");
    $("#campoCelular").inputmask("99-9-9999-9999");
});

var resizing = false;
moveNavigation();
$(window).on('resize', function(){
 if( !resizing ) {
 	window.requestAnimationFrame(moveNavigation);
 	resizing = true;
 }
});
 
function moveNavigation(){
 var mq = checkMQ(); //this function returns mobile,tablet or desktop 
 
 if ( mq == 'mobile' && topNavigation.parents('.cd-side-nav').length == 0 ) { //topNavigation = $('.cd-top-nav')
 detachElements();
 topNavigation.appendTo(sidebar); //sidebar = $('.cd-side-nav')
 searchForm.prependTo(sidebar);
 } else if ( ( mq == 'tablet' || mq == 'desktop') && topNavigation.parents('.cd-side-nav').length > 0 ) {
 detachElements();
 searchForm.insertAfter(header.find('.cd-logo')); //header = $('.cd-main-header')
 topNavigation.appendTo(header.find('.cd-nav'));
 }
 resizing = false;
}
 
function detachElements() {
 topNavigation.detach();//topNavigation = $('.cd-top-nav')
 searchForm.detach();//searchForm = $('.cd-search')
}