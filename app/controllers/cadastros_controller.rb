class CadastrosController < PrivateController
    before_filter :check_for_cancel, :only => [:create, :update]
    
    def index
      @cadastros = Cadastro.filtro(params.slice(:nome, :cpf, :rg)) #if params_no_empty?
    end
    
    def new
      @cadastro = Cadastro.new
    end
    
    def show
       @cadastro = Cadastro.find(params[:id])
    end
    
    def create
      @cadastro = Cadastro.new(cadastro_params)
      if @cadastro.save
        flash.now[:success] = "Ótimo! O novo cadastro está registrado."
        redirect_to @cadastro
      else
        render action: 'new'
      end 
    end
    
    def edit
      @cadastro = Cadastro.find(params[:id])
      @cadastro.data_de_nascimento = @cadastro.data_de_nascimento.strftime('%d/%m/%Y')
    end
    
    def update
      @cadastro = Cadastro.find(params[:id])
      if @cadastro.update_attributes(cadastro_params)
        flash.now[:success] = "As alterações foram salvas."
        redirect_to action: 'show', id: @cadastro
      else
        render 'edit'
      end
    end
    
    def destroy
      @cadastro = Cadastro.find params[:id]
      @cadastro.destroy
      redirect_to cadastros_path
      flash.now[:success] = "Excelente! O cadastro foi deletado."
    end
    
    def check_for_cancel
      redirect_to cadastros_path if params[:commit] == "Cancelar"
    end
    
    private
    
    def cadastro_params
      params.require(:cadastro).permit(:nome, :cpf, :sexo, :rg, :profissao, :endereco,
      :complemento, :data_de_nascimento, :telefone_fixo, :telefone_celular,
      :email, :cardiaco, :diabetes, :pressao, :gestante, :previsao_parto, :fumante, 
      :medicacao_em_uso)
    end
    
    def params_no_empty?
      params[:cpf].present? || params[:nome].present? || params[:rg].present?
    end
end