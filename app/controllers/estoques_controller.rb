class EstoquesController < PrivateController
    
    def index
        @estoques = Estoque.filtro(params.slice(:codigo_barras, :nome)).paginate(page: params[:page], per_page: 10)
    end
    
    def new
        @estoque = Estoque.new
    end
    
    def create
        @estoque = Estoque.new(estoque_params)
        if @estoque.save
            flash.now[:success] = "O novo produto foi registrado."
            redirect_to estoques_path
        else
            render action: 'new'
        end 
    end
    
    def show
        @estoque = Estoque.find(params[:id])
    end
    
    def edit
        @estoque = Estoque.find(params[:id])
    end
    
    def update
        @estoque = Estoque.find(params[:id])
        if @estoque.update_attributes(estoque_params)
            flash.now[:success] = "As alterações foram salvas."
            redirect_to action: 'show', id: @estoque
        else
            render 'edit'
        end
    end
    
    def destroy
        @estoque = Estoque.find params[:id]
        redirect_to estoques_path if @estoque.destroy
        flash.now[:success] = "O produto foi excluído."
    end
    
    private
    
    def estoque_params
       params.require(:estoque).permit(:nome, :codigo_barras, :descricao, :quantidade, :validade, :custo_unidade) 
    end
end
