class FirstPageController < ApplicationController
  def inicio
    redirect_to painel_path if logged_in?
  end
end
