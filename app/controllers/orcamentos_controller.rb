class OrcamentosController < CadastrosController
    
    def new
        @orcamento = Orcamento.new
        @cadastro = Cadastro.find(params[:cadastro_id])
    end
    
    def create
        @cadastro = Cadastro.find(params[:cadastro_id])
        @orcamento = @cadastro.orcamentos.build orcamento_params
        if @orcamento.save
            flash.now[:success] = "Feito!"
            redirect_to @orcamento
        else
            render "new"
        end
    end
    
    def destroy
            @cadastro = Cadastro.find(params[:cadastro_id])
            @orcamento = @cadastro.orcamentos.find(params[:orcamento_id])
            @orcamento.destroy
            
            respond_to do |format|
                format.js { head :ok }
            end
    end
    
    def index
        @cadastro = Cadastro.find(params[:cadastro_id])
        @orcamentos = @cadastro.orcamentos.paginate(page: params[:page], per_page: 10)
    end
    
    private
    
    def orcamento_params
        params.require(:orcamento).permit(
            :exodontia_qtd, :exodontia_preco,
            :cirurgia_qtd, :cirurgia_preco,
            :restauracao_qtd, :restauracao_preco,
            :restauracao_estetica_qtd, :restauracao_estetica_preco,
            :selante_qtd, :selante_preco,
            :polimento_qtd, :polimento_preco,
            :tratamento_de_canal_qtd, :tratamento_de_canal_preco,
            :canal_de_molar_qtd, :canal_de_molar_preco,
            :remocao_de_tartaro_qtd, :remocao_de_tartaro_preco,
            :limpeza_qtd, :limpeza_preco,
            :aplicacao_de_fluor_qtd, :aplicacao_de_fluor_preco,
            :bloco_qtd, :bloco_preco,
            :pino_com_coroa_de_porcelana_qtd, :pino_com_coroa_de_porcelana_preco,
            :pivo_acrilico_qtd, :pivo_acrilico_preco,
            :protese_parcial_removivel_qtd, :protese_parcial_removivel_preco,
            :protese_de_silicone_flex_qtd, :protese_de_silicone_flex_preco,
            :protese_total_qtd, :protese_total_preco,
            :protese_fixa_qtd, :protese_fixa_preco,
            :clareamento_qtd, :clareamento_preco,
            :raio_x_qtd, :raio_x_preco,
            :emergencia_qtd, :emergencia_preco,
            :aparelho_ortodontico_manutencao_qtd, :aparelho_ortodontico_manutencao_preco,
            :aparelho_estetico_safira_qtd, :aparelho_estetico_safira_preco,
        )
    end
end
