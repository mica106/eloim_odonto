class SessionsController < ApplicationController
  def new
    redirect_to painel_path if logged_in?
  end
  
  def create
    usuario = Usuario.find_by(usuario: params[:session][:usuario].downcase)
    if usuario && usuario.authenticate(params[:session][:password])
      entrar usuario
      redirect_to painel_path
    else
      flash.now[:danger] = 'Combinação usuário/senha incorreta. Tente novamente.'
      render 'new'
    end
  end
  
  def destroy
    sair
    redirect_to root_url
  end
end
