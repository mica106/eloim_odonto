module SessionsHelper
    def entrar(user)
       session[:usuario_id] = user.id
    end
    
    def current_usuario
        @current_usuario ||= Usuario.find_by(id: session[:usuario_id])
    end
    
    def sair
        session.delete(:usuario_id)
        @current_usuario = nil
    end
    
    def logged_in?
        !current_usuario.nil?
    end
    
    def logged_in_user
      unless logged_in?
        flash[:danger] = "Por favor, entre primeiro."
        redirect_to entrar_url
      end
    end
    
    # Stores the URL trying to be accessed.
    def store_location
        session[:forwarding_url] = request.url if request.get?
    end
end
