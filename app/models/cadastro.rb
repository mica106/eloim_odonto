class Cadastro < ActiveRecord::Base
    has_many :orcamentos, dependent: :destroy
    
    # todas as validações nesta seção:
    before_save { email.downcase! }
    before_validation { cpf.delete! '.-' }
    before_validation { telefone_fixo.delete! '()-' }
    before_validation { telefone_celular.delete! '()-' }
    
    validates :nome, presence: true, length: { maximum: 50 } 
    validates :cpf, uniqueness: true, allow_blank: true, length: { is: 11 }
    validates :sexo, presence: true
    validates :rg, presence: true, uniqueness: true
    validates :profissao, length: { maximum: 50 }
    validates :endereco, presence: true, length: { maximum: 200 }
    validates :complemento, length: { maximum: 100 }
    validates :data_de_nascimento, presence: true
    validates :telefone_fixo, length: { maximum: 11 }
    validates :telefone_celular, uniqueness: true, length: { maximum: 11 }
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
    validates :cardiaco, length: { maximum: 140 }
    validates :pressao, presence: true
    validates :medicacao_em_uso, length: { maximum: 140 }
    validate :gestante_validate
    
    def gestante_validate
        errors.add(:gestante, "não pode ser do sexo masculino") if sexo == 'Masculino' && gestante
        errors.add(:previsao_parto, "precisa ser informado.") if (sexo == 'Feminino' && gestante && previsao_parto.nil?)
        errors.add(:gestante, "precisa ser selecionado se mês de gestação foi informado.") if (sexo == 'Feminino' && !gestante && !previsao_parto.nil?)
    end
    # fim de validações
    
    # sistema de busca para cadastro de clientes
    scoped_search :on => [:nome, :cpf, :rg] #campo de busca 
    
    def boolean_convert boolean
        boolean ? "Sim" : "Não"
    end
    
    def self.filtro atributos
        resultados = self.where nil
        atributos.each do |chave, valor|
            resultados = self.search_for(valor) if valor.present?
        end
        resultados
    end
    
    # define um callback de envio de email
    #after_create :envia_email_boasvindas 
    def envia_email_boasvindas
        ClienteMailer.boas_vindas(self).deliver
    end

end