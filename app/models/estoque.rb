class Estoque < ActiveRecord::Base
    validates :nome, presence: true, length: {maximum: 30}
    validates :codigo_barras, presence: true, uniqueness: true, length: {maximum: 13}
    validates :descricao, length: {maximum: 50}
    validates :custo_unidade, presence: true
    
    scoped_search :on => [:nome, :codigo_barras]
    
    def self.filtro atributos
        resultados = self.where nil
        atributos.each do |chave, valor|
            resultados = self.search_for(valor) if valor.present?
        end
        resultados
    end
    
end
