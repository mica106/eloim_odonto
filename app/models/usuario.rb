class Usuario < ActiveRecord::Base
    has_secure_password
    
    validates :usuario, presence: true, length: {maximum: 20}
    validates :password, presence: true, length: {minimum: 6, maximum: 15}
end
