class CreateCadastros < ActiveRecord::Migration
  def change
    create_table :cadastros do |t|
      t.string :nome
      t.string :cpf
      t.string :sexo
      t.string :rg
      t.string :profissao
      t.string :endereco
      t.string :complemento
      t.string :data_de_nascimento
      t.string :telefone_fixo
      t.string :telefone_celular
      t.string :email
      t.string :cardiaco
      t.boolean :diabetes
      t.string :pressao
      t.boolean :gestante
      t.integer :semana
      t.boolean :fumante
      t.string :medicacao_em_uso
      
      t.timestamps null: false
    end
  end
end
