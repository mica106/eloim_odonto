class AlteraFormatoDeAlgumasVariaveisToCadastros < ActiveRecord::Migration
  def change
    change_table :cadastros do |t|
      t.remove :data_de_nascimento
      t.date :data_de_nascimento
    end
  end
end
