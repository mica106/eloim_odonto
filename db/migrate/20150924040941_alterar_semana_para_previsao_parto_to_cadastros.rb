class AlterarSemanaParaPrevisaoPartoToCadastros < ActiveRecord::Migration
  def change
    change_table :cadastros do |t|
      t.remove :semana
      t.date :previsao_parto
    end
  end
end
