class ChangePrevisaoPartoForIntegerToCadastros < ActiveRecord::Migration
  def change
    change_table :cadastros do |t|
      t.remove :previsao_parto
      t.integer :previsao_parto
    end
  end
end
