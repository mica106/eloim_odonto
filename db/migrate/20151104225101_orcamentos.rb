class Orcamentos < ActiveRecord::Migration
  def change
    create_table :orcamentos do |t|
      t.float :exodontia, array: true
    	t.float :cirurgia, array: true
    	t.float :restauracao, array: true
    	t.float :restauracao_estetica, array: true
    	t.float :selante, array: true
    	t.float :polimento, array: true
    	t.float :tratamento_de_canal, array: true
    	t.float :canal_de_molar, array: true
    	t.float :remocao_de_tartaro, array: true
    	t.float :limpeza, array: true
    	t.float :aplicacao_de_fluor, array: true
    	t.float :bloco, array: true
    	t.float :pino_com_coroa_de_porcelana, array: true
    	t.float :pivo_acrilico, array: true
    	t.float :protese_parcial_removivel, array: true
    	t.float :protese_de_silicone_flex, array: true
    	t.float :protese_total, array: true
    	t.float :protese_fixa, array: true
    	t.float :clareamento , array: true
    	t.float :raio_x, array: true
    	t.float :emergencia, array: true
    	t.float :aparelho_ortodontico_manutencao, array: true
    	t.float :aparelho_estetico_safira, array: true
    	
    	t.timestamps
    	end
    end
end
