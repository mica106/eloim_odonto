class AddOrcamentoToCadastro < ActiveRecord::Migration
  def change
    add_reference :cadastros, :orcamento, index: true, foreign_key: true
  end
end
