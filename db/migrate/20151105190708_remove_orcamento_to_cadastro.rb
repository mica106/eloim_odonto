class RemoveOrcamentoToCadastro < ActiveRecord::Migration
  def change
    remove_reference :cadastros, :orcamento, index: true, foreign_key: true
  end
end
