class AddCadastroToOrcamento < ActiveRecord::Migration
  def change
    add_reference :orcamentos, :cadastro, index: true, foreign_key: true
  end
end
