class ChangeTableOrcamento < ActiveRecord::Migration
  def change
    change_table :orcamentos do |t|
      t.remove :exodontia
    	t.remove :cirurgia
    	t.remove :restauracao
    	t.remove :restauracao_estetica
    	t.remove :selante
    	t.remove :polimento
    	t.remove :tratamento_de_canal
    	t.remove :canal_de_molar
    	t.remove :remocao_de_tartaro
    	t.remove :limpeza
    	t.remove :aplicacao_de_fluor
    	t.remove :bloco
    	t.remove :pino_com_coroa_de_porcelana
    	t.remove :pivo_acrilico
    	t.remove :protese_parcial_removivel
    	t.remove :protese_de_silicone_flex
    	t.remove :protese_total
    	t.remove :protese_fixa
    	t.remove :clareamento 
    	t.remove :raio_x
    	t.remove :emergencia
    	t.remove :aparelho_ortodontico_manutencao
    	t.remove :aparelho_estetico_safira
    end
    
    change_table :orcamentos do |t| 
      t.float :exodontia_qtd
    	t.float :cirurgia_qtd
    	t.float :restauracao_qtd
    	t.float :restauracao_estetica_qtd
    	t.float :selante_qtd
    	t.float :polimento_qtd
    	t.float :tratamento_de_canal_qtd
    	t.float :canal_de_molar_qtd
    	t.float :remocao_de_tartaro_qtd
    	t.float :limpeza_qtd
    	t.float :aplicacao_de_fluor_qtd
    	t.float :bloco_qtd
    	t.float :pino_com_coroa_de_porcelana_qtd
    	t.float :pivo_acrilico_qtd
    	t.float :protese_parcial_removivel_qtd
    	t.float :protese_de_silicone_flex_qtd
    	t.float :protese_total_qtd
    	t.float :protese_fixa_qtd
    	t.float :clareamento_qtd
    	t.float :raio_x_qtd
    	t.float :emergencia_qtd
    	t.float :aparelho_ortodontico_manutencao_qtd
    	t.float :aparelho_estetico_safira_qtd
      t.float :exodontia_preco
    	t.float :cirurgia_preco
    	t.float :restauracao_preco
    	t.float :restauracao_estetica_preco
    	t.float :selante_preco
    	t.float :polimento_preco
    	t.float :tratamento_de_canal_preco
    	t.float :canal_de_molar_preco
    	t.float :remocao_de_tartaro_preco
    	t.float :limpeza_preco
    	t.float :aplicacao_de_fluor_preco
    	t.float :bloco_preco
    	t.float :pino_com_coroa_de_porcelana_preco
    	t.float :pivo_acrilico_preco
    	t.float :protese_parcial_removivel_preco
    	t.float :protese_de_silicone_flex_preco
    	t.float :protese_total_preco
    	t.float :protese_fixa_preco
    	t.float :clareamento_preco
    	t.float :raio_x_preco
    	t.float :emergencia_preco
    	t.float :aparelho_ortodontico_manutencao_preco
    	t.float :aparelho_estetico_safira_preco
    end
    
  end
end
