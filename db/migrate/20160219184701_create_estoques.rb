class CreateEstoques < ActiveRecord::Migration
  def change
    create_table :estoques do |t|
      t.string :nome
      t.string :codigo_barras
      t.string :descricao
      t.integer :quantidade
      t.date :validade
      t.float :custo_unidade

      t.timestamps null: false
    end
  end
end
