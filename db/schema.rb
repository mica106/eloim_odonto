# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160219184701) do

  create_table "cadastros", force: :cascade do |t|
    t.string   "nome"
    t.string   "cpf"
    t.string   "sexo"
    t.string   "rg"
    t.string   "profissao"
    t.string   "endereco"
    t.string   "complemento"
    t.string   "telefone_fixo"
    t.string   "telefone_celular"
    t.string   "email"
    t.string   "cardiaco"
    t.boolean  "diabetes"
    t.string   "pressao"
    t.boolean  "gestante"
    t.boolean  "fumante"
    t.string   "medicacao_em_uso"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.date     "data_de_nascimento"
    t.integer  "previsao_parto"
  end

  create_table "estoques", force: :cascade do |t|
    t.string   "nome"
    t.string   "codigo_barras"
    t.string   "descricao"
    t.integer  "quantidade"
    t.date     "validade"
    t.float    "custo_unidade"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "orcamentos", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cadastro_id"
    t.float    "exodontia_qtd"
    t.float    "cirurgia_qtd"
    t.float    "restauracao_qtd"
    t.float    "restauracao_estetica_qtd"
    t.float    "selante_qtd"
    t.float    "polimento_qtd"
    t.float    "tratamento_de_canal_qtd"
    t.float    "canal_de_molar_qtd"
    t.float    "remocao_de_tartaro_qtd"
    t.float    "limpeza_qtd"
    t.float    "aplicacao_de_fluor_qtd"
    t.float    "bloco_qtd"
    t.float    "pino_com_coroa_de_porcelana_qtd"
    t.float    "pivo_acrilico_qtd"
    t.float    "protese_parcial_removivel_qtd"
    t.float    "protese_de_silicone_flex_qtd"
    t.float    "protese_total_qtd"
    t.float    "protese_fixa_qtd"
    t.float    "clareamento_qtd"
    t.float    "raio_x_qtd"
    t.float    "emergencia_qtd"
    t.float    "aparelho_ortodontico_manutencao_qtd"
    t.float    "aparelho_estetico_safira_qtd"
    t.float    "exodontia_preco"
    t.float    "cirurgia_preco"
    t.float    "restauracao_preco"
    t.float    "restauracao_estetica_preco"
    t.float    "selante_preco"
    t.float    "polimento_preco"
    t.float    "tratamento_de_canal_preco"
    t.float    "canal_de_molar_preco"
    t.float    "remocao_de_tartaro_preco"
    t.float    "limpeza_preco"
    t.float    "aplicacao_de_fluor_preco"
    t.float    "bloco_preco"
    t.float    "pino_com_coroa_de_porcelana_preco"
    t.float    "pivo_acrilico_preco"
    t.float    "protese_parcial_removivel_preco"
    t.float    "protese_de_silicone_flex_preco"
    t.float    "protese_total_preco"
    t.float    "protese_fixa_preco"
    t.float    "clareamento_preco"
    t.float    "raio_x_preco"
    t.float    "emergencia_preco"
    t.float    "aparelho_ortodontico_manutencao_preco"
    t.float    "aparelho_estetico_safira_preco"
  end

  add_index "orcamentos", ["cadastro_id"], name: "index_orcamentos_on_cadastro_id"

  create_table "usuarios", force: :cascade do |t|
    t.string   "usuario"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

end
