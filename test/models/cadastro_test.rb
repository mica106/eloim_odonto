
require 'test_helper'

class CadastroTest < ActiveSupport::TestCase
  def setup
    @cadastro = Cadastro.new(nome: "Micael Lopes da Silva", cpf: "05738839390", sexo: "M", rg: "0363750720082", profissao: "Analista de Sistemas", 
    endereco: "Travessa da Vitória 19 Anjo da Guarda, São Luís - MA", complemento: "", data_de_nascimento: "1995-06-21", telefone_fixo: "",
    telefone_celular: "98989097710", email: "micaelopes32@gmail.com", cardiaco: "", diabetes: false, pressao: "normal", fumante: false,
    medicacao_em_uso: "")
  end
  
  test "email addresses should be unique" do
    duplicate_user = @cadastro.dup
    duplicate_user.email = @cadastro.email.upcase
    @cadastro.save
    assert_not duplicate_user.valid?
  end
  
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @cadastro.email = invalid_address
      assert_not @cadastro.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
end
